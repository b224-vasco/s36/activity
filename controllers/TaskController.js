// Controllers contain the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file


// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require('../models/Task.js')

// Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and exports these functions
module.exports.getAllTasks = () => {
	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then(result => {
		// The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result
	})
}

// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})
	// Saves the newly created "newTask" object in the MongoDB database
	// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
	return newTask.save().then((savedTask, error) => {
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(error) {
			console.log(error)
			return false
		} else {
			return savedTask //'Task created successfully!'
		}
		
	})
}	


// Updates a task on the MongoDb collection
// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "newContent"
module.exports.updateTask = (taskId, newContent) => {
	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// "findById" is the same as "find({"_id" : value})"
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		}
		result.name = newContent.name
		// Saves the updated object in the MongoDB database
		return result.save().then((updatedTask, error) => {
			if(error) {
				console.log(error)
				return false
			} else {
				return updatedTask
			}
			
		})
	})
}



// Delete a task from the MongoDb collection
module.exports.deleteTask = (taskId) => {
	// The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
	// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
		if(error) {
			return(error)
		}
		return deletedTask
	})
}




// Activity
// Function for getting one the tasks from our DB.
module.exports.getOneTasks = (taskId) => {
	return Task.findById(taskId).then((getOneTasks, error) => {
		if(error) {
			return(error)
		}
		console.log(getOneTasks)
		return getOneTasks

	})
}

module.exports.updateStatus = (taskId, newContent) => {
	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// "findById" is the same as "find({"_id" : value})"
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		}
		result.status = newContent.status
		// Saves the updated object in the MongoDB database
		return result.save().then((updatedStatus, error) => {
			if(error) {
				console.log(error)
				return false
			} else {
				return updatedStatus
			}
			
		})
	})
}
