
// Import express and any controllers using the 'require' directive.
// Contains all the endpoints for our application
// We separate the routes such that "app.js" only contains information on the server
// We need to use express' Router() function to achieve this
const express = require('express')

// The "taskController" allows us to use the functions defined in the "taskController.js" file
const TaskController = require('../controllers/TaskController.js')

// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router()


// [Section] Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller

  

// Route for /tasks/ - runs the getAllTasks function from the controller.
router.get('/', (req, res) => {
    // "resultFromController" is only used here to make the code easier to understand but it's common practice to use the shorthand parameter name for a result using the parameter name "result"/"res"
    TaskController.getAllTasks().then((resultFromController) => res.send(resultFromController))
})


// Route for /tasks/create - runs the createTask function from the controller.
router.post('/create', (req, res) =>{
    // The "createTask" function needs the data from the request body, so we need to supply it to the function
    TaskController.createTask(req.body).then((resultFromController) => res.send(resultFromController))
})


// Route for /tasks/update - runs the updateTask function from the controller.
// You can use a parameter variable within the endpoint by using a ':' and the name of the parameter. Ex.: ':id'
router.put('/:id/update', (req, res) => {
    // The "updateTask" function will accept the following 2 arguments:
        // "req.params.id" retrieves the task ID from the parameter
        // "req.body" retrieves the data of the updates that will be applied to a task from the request's "body" property
    TaskController.updateTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
})


// Route for /tasks/delete - runs the deleteTask function from the controller.
router.delete('/:id/delete', (req, res) => {
    TaskController.deleteTask(req.params.id).then((resultFromController) => res.send(resultFromController))
})


// Activity
router.get("/:id", (req, res) => {
    TaskController.getOneTasks(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
});

// Route for /tasks/update - runs the updateTask function from the controller.
// You can use a parameter variable within the endpoint by using a ':' and the name of the parameter. Ex.: ':id'
router.put('/:id/complete', (req, res) => {
    // The "updateTask" function will accept the following 2 arguments:
        // "req.params.id" retrieves the task ID from the parameter
        // "req.body" retrieves the data of the updates that will be applied to a task from the request's "body" property
    TaskController.updateStatus(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
})

// Use "module.exports" to export the router object to use in the "app.js"
module.exports = router




